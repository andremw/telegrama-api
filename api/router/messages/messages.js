const socketIO = require('socket.io');

module.exports = (server) => {
  const io = socketIO.listen(server.listener);

  const { botApi: bot, pubsub } = server.app;

  server.route({
    method: 'GET',
    path: '/messages',
    handler(req, reply) {
      reply([]);
    },
  });

  server.route({
    method: 'POST',
    path: '/messages',
    handler(req, reply) {
      pubsub.publishSync('new:bot-message', req.payload);
      bot.sendMessage(req.payload.chat_id, req.payload.text);
      reply({});
    },
    config: {
      validate: {
        params(value, options, next) {
          const payload = options.context.payload || {};
          if (!payload.chat_id) {
            return next(new Error('Request failed because "chat_id" is required'));
          }
          if (!payload.text) {
            return next(new Error('Request failed because "text" is required'));
          }
          return next();
        },
      },
    },
  });

  bot.on('message', (msg) => {
    const data = {
      text: msg.text,
      time: new Date().getTime(),
      id: msg.message_id,
      chatId: msg.chat.id,
      userId: msg.from.id,
      name: `${msg.from.first_name} ${msg.from.last_name}`,
      username: msg.from.username,
    };

    bot.getUserProfilePhotos(msg.from.id).then((userProfilePhotos) => {
      const filePath = userProfilePhotos.photos[0][0].file_path;
      const botToken = server.app.config.botToken;
      data.userProfilePhoto = `https://api.telegram.org/file/bot${botToken}/${filePath}`;
      io.emit('new:user-message', data);
    });
  });
};
