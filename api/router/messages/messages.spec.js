const nock = require('nock');

const server = require('../../../api');

describe('/messages', () => {
  let options = null;
  const BOT_TOKEN = server.app.config.botToken;

  beforeEach(() => {
    options = {
      method: 'POST',
      url: '/messages',
      payload: {},
    };
  });

  it('POST not allowed without "chat_id" and "text"', () => server.inject(options).then((res) => {
    const payload = JSON.parse(res.payload);
    expect(res.statusCode).toEqual(400);
    expect(payload.message).toEqual('Request failed because "chat_id" is required');
  }).then(() => {
    options.payload.chat_id = 123456;

    return server.inject(options).then((res) => {
      const payload = JSON.parse(res.payload);
      expect(res.statusCode).toEqual(400);
      expect(payload.message).toEqual('Request failed because "text" is required');
    });
  }));

  it('POST triggers a pubsub "new:bot-message" event if successful', () => {
    options.payload.chat_id = 123456;
    options.payload.text = 'robot-input-text';

    nock(`https://api.telegram.org/bot${BOT_TOKEN}`)
      .post('/sendMessage')
      .reply(200, {
        ok: true,
        result: '',
      });

    const spy = jasmine.createSpy();

    server.app.pubsub.subscribe('new:bot-message', spy);

    return server.inject(options).then(() => {
      expect(spy).toHaveBeenCalledWith('new:bot-message', {
        chat_id: 123456,
        text: 'robot-input-text',
      });
    });
  });

  it('POST makes a call to telegram\'s API to send the bot message to the user', () => {
    options.payload.chat_id = 123456;
    options.payload.text = 'robot-input-text';

    nock(`https://api.telegram.org/bot${BOT_TOKEN}`)
      .post('/sendMessage')
      // this object is required otherwise the bot-api won't be able to parse the response
      .reply(200, {
        ok: true,
        result: '',
      });

    return server.inject(options).then((res) => {
      expect(res.statusCode).toEqual(200);
    });
  });
});
