const Hapi = require('hapi');
const pubsub = require('pubsub-js');
const TelegramBotApi = require('node-telegram-bot-api');

const config = require('../config');

const prepareRoutes = require('./router');

const shouldPoll = config.NODE_ENV && config.NODE_ENV !== 'test';
const botApi = new TelegramBotApi(config.botToken, { polling: shouldPoll });

const server = new Hapi.Server();
server.connection({
  port: config.server.port,
  host: 'localhost',
});

server.app.pubsub = pubsub;
server.app.config = config;
server.app.botApi = botApi;

prepareRoutes(server);

server.start((err) => {
  if (err) {
    throw err;
  }
});

module.exports = server;
