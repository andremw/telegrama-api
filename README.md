# Telegrama-API
----
#### Telegrama-API is responsible for connecting the Bot to Telegram's BOT API.

### Quick Start
1. Clone the repo and go to the root of the project
2. Run `yarn`
2. In the root of the project run `PORT=[port] BOT_TOKEN=[token] node .`

### Linting
Run `yarn lint`

### Unit tests
Run `yarn test`

### Before building
Run `yarn build`

Telegrama-API is powered by HapiJS, node-telegram-bot-api and Socket.io.
