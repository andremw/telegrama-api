module.exports = {
  server: {
    port: process.env.PORT || 3000,
  },
  botToken: process.env.BOT_TOKEN,
  NODE_ENV: process.env.NODE_ENV,
};
